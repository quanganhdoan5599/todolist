/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary-purple': '#5529DC',
        'malibu-blue': '#7ECDF4',
        'light-gray': '#B0B0B0',
        'silver': '#CECECE',
        'dark-gray': '#D9D9D9',
        'ghost-white': '#F9F9F9',
        'salmon': '#F47E7E',
        'soft-gray': '#ECECEC',
      },
      dropShadow: {
        'glow': 'drop-shadow(0 0 0.75rem crimson)'
      },
      backgroundColor: {
        'primary-purple-background': 'linear-gradient(90deg, rgba(85,41,220,1) 0%, rgba(254,254,254,1) 100%)'
      }
    },
  },
  plugins: [],
}

