import TodoListBody from "./components/templates/TodoListBody"


function App() {
  return (
    <>
      <TodoListBody />
    </>
  )
}

export default App
