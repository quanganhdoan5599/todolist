import { useState } from "react";
import { TWorkItem } from "../const";
import { todoApi } from "../api-clients/todo";

function useFetchTodoList() {
  const [todoList, setTodoList] = useState<TWorkItem[]>([])
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [initialLoading, setInitialLoading] = useState<boolean>(false)

  const fetchTodoList = () => {
    setIsLoading(true)
    todoApi.getTodoList().then((res) => {
      setTodoList(res.data)
      setIsLoading(false)
      setInitialLoading(true)
    }).catch((err) => {
      console.log(err)
      setIsLoading(false)
    })
  }

  return {todoList, fetchTodoList, isLoading, initialLoading}
}

export default useFetchTodoList