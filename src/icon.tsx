const deleteIcon = (
  <svg className="w-7 h-7 dark:text-white cursor-pointer" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m15 9-6 6m0-6 6 6m6-3a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
  </svg>
)

const checkedIcon = (
  <svg className="w-7 h-7 dark:text-white cursor-pointer" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24">
    <path fill-rule="evenodd" d="M2 12a10 10 0 1 1 20 0 10 10 0 0 1-20 0Zm13.7-1.3a1 1 0 0 0-1.4-1.4L11 12.6l-1.8-1.8a1 1 0 0 0-1.4 1.4l2.5 2.5c.4.4 1 .4 1.4 0l4-4Z" clip-rule="evenodd" />
  </svg>
)

const uncheckedIcon = (
  <svg className="w-7 h-7 dark:text-white cursor-pointer" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.5 11.5 11 14l4-4m6 2a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
  </svg>
)

const loadingIcon = (color: string, isSmall?: boolean) => (
  <svg
    className={`inline ${isSmall ? 'w-5 h-5' : 'w-6 h-6'} animate-spin`}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 120 120"
  >
    <circle
      cx="60"
      cy="60"
      r="52"
      fill="transparent"
      stroke="#f3f3f3"
      strokeWidth="16"
    />
    <circle
      cx="60"
      cy="60"
      r="52"
      fill="transparent"
      stroke={color}
      strokeWidth="16"
      strokeDasharray="327"
      strokeDashoffset="0"
      strokeLinecap="round"
    >
      <animate
        attributeName="stroke-dashoffset"
        dur="2s"
        keyTimes="0;1"
        values="0;327"
        repeatCount="indefinite"
      />
    </circle>
  </svg>
)

const saveIcon = (color: string) => (
  <svg className="w-[18px] h-[18px]" fill={color} viewBox="48 48 160 160" xmlns="http://www.w3.org/2000/svg">
    <g fill-rule="evenodd">
      <path d="M65.456 48.385c10.02 0 96.169-.355 96.169-.355 2.209-.009 5.593.749 7.563 1.693 0 0-1.283-1.379.517.485 1.613 1.67 35.572 36.71 36.236 37.416.665.707.241.332.241.332.924 2.007 1.539 5.48 1.539 7.691v95.612c0 7.083-8.478 16.618-16.575 16.618-8.098 0-118.535-.331-126.622-.331-8.087 0-16-6.27-16.356-16.1-.356-9.832.356-118.263.356-126.8 0-8.536 6.912-16.261 16.932-16.261zm-1.838 17.853l.15 121c.003 2.198 1.8 4.003 4.012 4.015l120.562.638a3.971 3.971 0 0 0 4-3.981l-.143-90.364c-.001-1.098-.649-2.616-1.445-3.388L161.52 65.841c-.801-.776-1.443-.503-1.443.601v35.142c0 3.339-4.635 9.14-8.833 9.14H90.846c-4.6 0-9.56-4.714-9.56-9.14s-.014-35.14-.014-35.14c0-1.104-.892-2.01-1.992-2.023l-13.674-.155a1.968 1.968 0 0 0-1.988 1.972zm32.542.44v27.805c0 1.1.896 2.001 2 2.001h44.701c1.113 0 2-.896 2-2.001V66.679a2.004 2.004 0 0 0-2-2.002h-44.7c-1.114 0-2 .896-2 2.002z" />
      <path d="M127.802 119.893c16.176.255 31.833 14.428 31.833 31.728s-14.615 31.782-31.016 31.524c-16.401-.259-32.728-14.764-32.728-31.544s15.735-31.963 31.91-31.708zm-16.158 31.31c0 9.676 7.685 16.882 16.218 16.843 8.534-.039 15.769-7.128 15.812-16.69.043-9.563-7.708-16.351-15.985-16.351-8.276 0-16.045 6.52-16.045 16.197z" />
    </g>
  </svg>
)

const cancelIcon = (
  <svg className="w-6 h-6 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18 18 6m0 12L6 6" />
  </svg>
)

const editIcon = (
  <svg fill="#fff" className="w-5 h-5" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
    <g>
      <rect x="129.177" y="254.498" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -161.9856 228.3968)" width="131.06" height="110.469" />
    </g>
    <g>
      <path d="M92.531,347.45l-71.281,42.037L0,504.437l114.951-21.249l42.037-71.282L92.531,347.45z M80.187,447.865
      c-6.519,6.52-17.091,6.52-23.611,0s-6.52-17.091,0-23.611c6.52-6.52,17.091-6.52,23.611,0
      C86.707,430.774,86.707,441.345,80.187,447.865z"/>
    </g>
    <g>
      <path d="M468.181,128.393l9.648-9.648c25.401-25.401,25.401-66.73,0-92.131c-25.401-25.401-66.73-25.401-92.131,0L218.591,193.718
      l92.132,92.132l133.389-133.389l19.751,19.751L300.806,335.269l24.068,24.068L512,172.212L468.181,128.393z"/>
    </g>
  </svg>
)

export {
  deleteIcon,
  editIcon,
  uncheckedIcon,
  checkedIcon,
  cancelIcon,
  saveIcon,
  loadingIcon
}