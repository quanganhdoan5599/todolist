export const priorityList = [
    {
        id: 0,
        value: 'emergency',
    },
    {
        id: 1,
        value: 'important',
    },
    {
        id: 2,
        value: 'urgent',
    }
]

export interface TActive {
    emergencyActive: boolean,
    importantActive: boolean,
    urgentActive: boolean
}

export interface TWorkItem {
    id?: string
    description: string,
    type: string,
    finish: boolean
}

export const CHECK_EMPTY_INPUT = /\S/;