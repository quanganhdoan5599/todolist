import { FC } from "react"
import logo from '../../../assets/logo.svg'
import { Heading } from "../../atoms/Text/Heading"

interface Props {
    className?: string
}

const Logo: FC<Props> = (props) => {
    const {className} = props
    return(
    <div className={`flex justify-center items-center ${className}`}>
        <img src={logo} alt="logo" className="w-24 h-auto"/>
        <Heading content="TODO List" className="drop-shadow-lg text-primary-purple"/>
    </div>
    )
}

export default Logo