import type { FC } from 'react'

const Loading: FC = () => {
  return (
    <div className="relative">
      <div className="flex justify-center w-full h-full absolute z-50">
        <div className="flex flex-col">
          <div className="flex space-x-24">
            <div className="container space-y-10 relative">
              <div className="flex flex-col">
                <div className="flex flex-row space-x-4">
                  <div className="w-12 h-12 rounded-full animate-spin border-2 border-solid border-primary-purple border-t-transparent"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Loading