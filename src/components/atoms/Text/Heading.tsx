import { FC } from "react"

interface Props {
    content: string
    className?: string
}

export const Heading: FC<Props> = (props) => {
    const {content, className} = props
    return(
        <h1 className={`text-4xl text-center font-semibold ${className}`}>{content}</h1>
    )
}

Heading.displayName = 'Heading'