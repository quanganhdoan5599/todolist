import React from 'react'

interface Props {
  className?: string
  textContent: string
}

export const ErrorText: React.FC<Props> = (props) => {
  const { className, textContent } = props
  return (
    <p className={`text-red-500 mt-2 font-normal text-[14px] ${className}`}>{textContent}</p>
  )
}
