import { FC } from "react";

interface Props {
    className?: string
    onClick?: () => void
}

export const AddButton: FC<Props> = (props) => {
    const { className, onClick } = props
    return (
        <button className={`flex justify-center items-center outline-none text-primary-purple ${className}`} onClick={onClick}>
            <span className="font-bold text-4xl px-2 hover:opacity-75">+</span>
        </button>
    )
}

AddButton.displayName = 'AddButton'