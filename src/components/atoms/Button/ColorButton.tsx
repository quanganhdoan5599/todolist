import { FC } from "react";
import { useEditActiveStore } from "../../../store/editActive";

interface Props {
    className?: string
    color: string
    onClick: () => void
}

export const ColorButton: FC<Props> = (props) => {
    const {className, color, onClick} = props
    const { editActive } = useEditActiveStore()
    return(
        <div className={`flex items-center cursor-pointer drop-shadow hover:scale-125 ${editActive ? 'pointer-events-none' : ''}`} onClick={onClick}>
            <div className={`${color} rounded-full p-2 ${className}`}></div>
        </div>
    )
}

ColorButton.displayName = 'AddButton'