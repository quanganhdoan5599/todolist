import React from "react"

interface Props {
    placeholder?: string
    className?: string
    disabled?: boolean
    value?: string
    onChange?: () => void
    onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void
}

export const Input= React.forwardRef<HTMLInputElement, Props>((props, ref) => {
    const {placeholder, className, disabled, value, onChange, onKeyDown} = props
    return(
        <input
            ref={ref} 
            type="text" 
            className={`w-full px-3 rounded text-black placeholder:text-silver h-[42px] border border-[#eeeeeeed] bg-white font-normal text-[15px] focus-visible:outline-none focus:border-blue-500 drop-shadow !font-sans ${className}`} placeholder={placeholder} 
            disabled={disabled} 
            value={value} 
            onChange={onChange}
            onKeyDown={onKeyDown}
        />
    )
})

Input.displayName = 'Input'
