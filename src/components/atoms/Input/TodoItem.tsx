import React from "react"

interface Props {
    className?: string
    onInput?: (e: React.ChangeEvent<HTMLDivElement>) => void
    children?: React.ReactNode
    contentEditable: boolean
    finished: boolean
    onClick?: () => void
    onKeyDown?: (e: React.KeyboardEvent<HTMLDivElement>) => void
}

export const TodoItem = React.forwardRef<HTMLDivElement, Props>((props, ref) => {
    const { className, children, contentEditable, onClick, onInput, onKeyDown, finished } = props

    return (
        <div
            ref={ref}
            className={`w-full flex break-all py-[9px] px-3 rounded text-white text-justify placeholder:text-silver min-h-[42px] border border-[#eeeeeeed] bg-white font-normal text-[15px] focus-visible:outline-none focus:border-blue-500 drop-shadow !font-sans ${finished ? 'line-through' : ''} ${className}`}
            contentEditable={contentEditable}
            onClick={onClick}
            onInput={onInput}
            onKeyDown={onKeyDown}
            suppressContentEditableWarning={true}
        >
            {children}
        </div>
    )
})

TodoItem.displayName = 'TodoItem'