import { FC, useEffect, useState } from "react"
import { TActive, priorityList } from "../../const"
import EmergencyWorkSection from "../organisms/EmergencyWorkSection"
import Logo from "../molecules/Logo/Logo"
import InputSection from "../organisms/InputSection"
import ImportantWorkSection from "../organisms/ImportantWorkSection"
import UrgentWorkSection from "../organisms/UrgentWorkSection"
import useFetchTodoList from "../../hooks/useFetchTodoList"
import { todoApi } from "../../api-clients/todo"
import Loading from "../molecules/Loading/Loading"

interface Props {

}

const TodoListBody: FC<Props> = () => {

  const [active, setActive] = useState<TActive>({
    emergencyActive: true,
    importantActive: true,
    urgentActive: true
  })

  const { todoList, fetchTodoList, isLoading, initialLoading } = useFetchTodoList()

  useEffect(() => {
    fetchTodoList()
  }, [])

  const handleDelete = (id: string) => {
    todoApi.deleteTodoItem(id).then(() => fetchTodoList())
      .catch((err) => console.log(err))
  };

  const handleCheck = (id: string) => {
    todoApi.getTodoItem(id)
      .then((res) => {
        const updatedDataItem = { ...res.data, finish: !res.data.finish };
        return todoApi.updateTodoItem(id, updatedDataItem);
      })
      .then(() => {
        fetchTodoList();
      })
      .catch((err) => console.log(err));
  }

  const handleUpdate = (id: string, description: string) => {
    todoApi.getTodoItem(id)
      .then((res) => {
        const updatedDataItem = { ...res.data, description: description };
        return todoApi.updateTodoItem(id, updatedDataItem);
      })
      .then(() => {
        fetchTodoList();
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <>
      {isLoading && !initialLoading && (
        <div className="flex w-screen h-screen items-center justify-center absolute bg-white opacity-50 z-50">
          <Loading />
        </div>
      )}
      <div className="flex flex-col justify-center items-center">
        <Logo className="mt-4" />
        <div className="mt-8 mx-4 lg:w-[700px] max-w-[700px]">
          <InputSection active={active} setActive={setActive} fetchTodoList={fetchTodoList} />
          <div className="flex flex-col mt-4 mb-4">
            <EmergencyWorkSection
              active={active.emergencyActive}
              emergencyList={todoList.filter(item => item.type === priorityList[0].value)}
              onDelete={handleDelete}
              onCheck={handleCheck}
              onUpdate={handleUpdate}
              className={active.emergencyActive ? 'fade-in' : 'fade-out'}
            />
            <ImportantWorkSection
              active={active.importantActive}
              importantList={todoList.filter(item => item.type === priorityList[1].value)}
              onDelete={handleDelete}
              onCheck={handleCheck}
              onUpdate={handleUpdate}
            />
            <UrgentWorkSection
              active={active.urgentActive}
              urgentList={todoList.filter(item => item.type === priorityList[2].value)}
              onDelete={handleDelete}
              onCheck={handleCheck}
              onUpdate={handleUpdate}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default TodoListBody