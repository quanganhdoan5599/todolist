import { FC, useEffect, useState } from "react"
import lightOff from '../../assets/light-off.png'
import lightOn from '../../assets/light-on.png'
import { TActive, TWorkItem, priorityList } from "../../const"
import { Input } from "../atoms/Input/Input"
import { ColorButton } from "../atoms/Button/ColorButton"
import { AddButton } from "../atoms/Button/AddButton"
import { todoApi } from "../../api-clients/todo"
import { Controller, SubmitHandler, useForm } from "react-hook-form"
import { ErrorText } from "../atoms/Text/ErrorText"
import { loadingIcon } from "../../icon"
import { useEditActiveStore } from "../../store/editActive"

interface Props {
    className?: string
    active: TActive,
    setActive: React.Dispatch<React.SetStateAction<TActive>>,
    fetchTodoList: () => void
}

const InputSection: FC<Props> = (props) => {

    const { className, active, setActive, fetchTodoList } = props

    const [isLightOn, setIsLightOn] = useState<boolean>(false)
    const [trueValuesCount, setTrueValuesCount] = useState<number>(1)
    const [todoType, setTodoType] = useState<string>('emergency')
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const { editActive } = useEditActiveStore()

    const formCreateTodo = useForm<TWorkItem>(
        {
            defaultValues: {
                description: '',
                type: todoType,
                finish: false
            }
        }
    )

    useEffect(() => {
        setTrueValuesCount(Object.values(active).filter(value => value === true).length)
    }, [active]);

    useEffect(() => {
        if (trueValuesCount === 1 && active.emergencyActive === true) {
            setTodoType(priorityList[0].value)
        }
        else if (trueValuesCount === 1 && active.importantActive === true) {
            setTodoType(priorityList[1].value)
        }
        else if (trueValuesCount === 1 && active.urgentActive === true) {
            setTodoType(priorityList[2].value)
        }
    }, [trueValuesCount, active])

    useEffect(() => {
        if (trueValuesCount === 1 && formCreateTodo.formState.errors.type?.message) {
            formCreateTodo.clearErrors('type')
        }
    }, [trueValuesCount, formCreateTodo])

    const handleMemo = () => {
        setIsLightOn(!isLightOn)
    }

    const handleSaveWork: SubmitHandler<TWorkItem> = (data) => {
        if (trueValuesCount === 1) {
            setIsLoading(true)
            todoApi.addTodoItem({ ...data, type: todoType }).then(() => {
                fetchTodoList()
                formCreateTodo.reset()
                setIsLoading(false)
            })
        }
        else formCreateTodo.setError('type', { type: 'validate', message: 'You must choose just one type of work' })

    }

    return (
        <>
            <div className={`flex gap-x-3 justify-center items-center w-full ${className}`}>
                <div className="flex justify-center relative w-full">
                    <Controller
                        rules={{
                            required: 'Your task is empty',
                        }}
                        control={formCreateTodo.control}
                        name="description"
                        render={({ field: { onChange, ref } }) => (
                            <Input
                                ref={ref}
                                className={`pr-36 ${editActive ? 'pointer-events-none' : ''}`}
                                placeholder='Add item...'
                                value={formCreateTodo.watch('description')}
                                onChange={onChange}
                                onKeyDown={(e: React.KeyboardEvent<HTMLInputElement>) => {
                                    if (e.key === "Enter") {
                                        e.preventDefault();
                                        formCreateTodo.handleSubmit(handleSaveWork)()
                                    }
                                }}
                            />
                        )}
                    />
                    <div className="flex gap-x-4 mr-3 absolute right-10 h-[42px]">
                        <ColorButton 
                            color={"bg-red-500"} 
                            className={`${active.emergencyActive ? 'border-[2.5px] border-blue-500' : ''}`} 
                            onClick={() => setActive({ ...active, emergencyActive: (trueValuesCount === 1 && active.emergencyActive === true) ? active.emergencyActive : !active.emergencyActive })} 
                        />
                        <ColorButton 
                            color={"bg-yellow-500"} 
                            className={`${active.importantActive ? 'border-[2.5px] border-blue-500' : ''}`} 
                            onClick={() => setActive({ ...active, importantActive: (trueValuesCount === 1 && active.importantActive === true) ? active.importantActive : !active.importantActive })} 
                        />
                        <ColorButton 
                            color={"bg-orange-500"} 
                            className={`${active.urgentActive ? 'border-[2.5px] border-blue-500' : ''}`} 
                            onClick={() => setActive({ ...active, urgentActive: (trueValuesCount === 1 && active.urgentActive === true) ? active.urgentActive : !active.urgentActive })} 
                        />
                    </div>
                    {isLoading ?
                        <div className="absolute right-0 flex items-center pl-2 pr-4 h-[42px]">
                            {loadingIcon('#7E5DE3')}
                        </div>
                        :
                        <AddButton
                            className={`absolute right-0 h-[42px] pr-2 rounded-r bg-transparent drop-shadow-sm ${editActive ? 'pointer-events-none !text-light-gray' : ''}`}
                            onClick={formCreateTodo.handleSubmit(handleSaveWork)}
                        />
                    }
                </div>
                <img
                    id="lightBulb"
                    src={isLightOn ? lightOn : lightOff}
                    alt="light"
                    className={`h-10 cursor-pointer drop-shadow ${isLightOn ? 'lightOn' : ''}`}
                    onClick={handleMemo}
                    onMouseOver={e => e.currentTarget.src = lightOn}
                    onMouseLeave={e => e.currentTarget.src = isLightOn ? lightOn : lightOff}
                />
            </div>
            {formCreateTodo.formState.errors.description?.message && <ErrorText textContent={formCreateTodo.formState.errors.description.message} />}
            {formCreateTodo.formState.errors.type?.message && !formCreateTodo.formState.errors.description?.message && <ErrorText textContent={formCreateTodo.formState.errors.type.message} />}
        </>
    )
}

export default InputSection