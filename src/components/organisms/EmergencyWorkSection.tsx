import { FC, useEffect, useState } from "react"
import { CHECK_EMPTY_INPUT, TWorkItem } from "../../const"
import { cancelIcon, checkedIcon, deleteIcon, editIcon, loadingIcon, saveIcon, uncheckedIcon } from "../../icon"
import { TodoItem } from "../atoms/Input/TodoItem"
import { useEditActiveStore } from "../../store/editActive"
import { todoApi } from "../../api-clients/todo"
// import { todoApi } from "../../api-clients/todo"

interface Props {
    active: boolean
    className?: string
    emergencyList: TWorkItem[]
    onDelete: (id: string) => void
    onCheck: (id: string) => void
    onUpdate: (id: string, description: string) => void
}

interface Loading {
    isFinish: boolean[],
    isDelete: boolean[],
    isSaving: boolean[]
}

const EmergencyWorkSection: FC<Props> = (props) => {

    const { active, className, emergencyList, onDelete, onCheck, onUpdate } = props

    const [loading, setLoading] = useState<Loading>({
        isFinish: [],
        isDelete: [],
        isSaving: []
    })
    const [contentEditable, setContentEditable] = useState<boolean[]>([])
    const [description, setDescription] = useState<string>('')
    const { editActive, setEditActive } = useEditActiveStore()
    const [finishLoadingItem, setFinishLoadingItem] = useState<boolean>(false)

    useEffect(() => {
        setLoading({ ...loading, isFinish: emergencyList.map(() => false), isDelete: emergencyList.map(() => false), isSaving: emergencyList.map(() => false) })
        setContentEditable(emergencyList.map(() => false))
        setEditActive(false)
    }, [emergencyList]);

    const handleDelete = (id: string) => {
        setLoading(() => ({
            ...loading,
            isDelete: {
                ...loading.isDelete,
                [id]: true,
            },
        }));
        onDelete(id)
    }

    const handleCheck = (id: string) => {
        setLoading(() => ({
            ...loading,
            isFinish: {
                ...loading.isFinish,
                [id]: true,
            },
        }));
        onCheck(id);
    };

    const handleEdit = (id: string) => {
        todoApi.getTodoItem(id).then((res) => {
            setDescription(res.data.description)
        })
        .then(() => setFinishLoadingItem(true))
        const updatedContentEditable = [...contentEditable];
        updatedContentEditable[Number(id)] = !updatedContentEditable[Number(id)];
        setContentEditable(updatedContentEditable);
        setEditActive(true)
    }

    const handleCancel = (id: string) => {
        const updatedContentEditable = [...contentEditable];
        updatedContentEditable[Number(id)] = !updatedContentEditable[Number(id)];
        setContentEditable(updatedContentEditable);
        setEditActive(false)
        setFinishLoadingItem(false)
    }

    const handleUpdateItem = (id: string) => {
        setLoading(() => ({
            ...loading,
            isSaving: {
                ...loading.isSaving,
                [id]: true,
            },
        }));
        if (CHECK_EMPTY_INPUT.test(description)) {
            onUpdate(id, description);
        }
        else {
            setLoading(() => ({
                ...loading,
                isSaving: {
                    ...loading.isSaving,
                    [id]: false,
                },
            }));
        }
        setFinishLoadingItem(false)
    }

    useEffect(() => {
        console.log(finishLoadingItem)
    }, [finishLoadingItem])

    const handleSavingIcon = (id: string) => {
        if (loading.isSaving[Number(id)])
            return (
                <div className="absolute text-white right-2 flex cursor-not-allowed items-center gap-2">
                    <span>{loadingIcon('#EF4444', true)}</span>
                    <span className="text-light-gray">{cancelIcon}</span>
                </div>
            )
        else return (
            <div className="absolute right-2 flex items-center gap-2">
                <span 
                    onClick={() => handleUpdateItem(id)} 
                    className={`${!finishLoadingItem ? 'pointer-events-none' : 'cursor-pointer'}`}
                >
                    {saveIcon('#EF4444')}
                </span>
                <span onClick={() => handleCancel(id)} className="text-red-500 cursor-pointer">{cancelIcon}</span>
            </div>
        )
    }


    return (
        <div className={`${active ? 'block' : 'hidden'} ${className}`}>
            {emergencyList.map(item => {
                return (
                    <div className="flex gap-x-2 mb-3" key={item.id}>
                        <div
                        className={`flex flex-1 items-center relative ${editActive && !contentEditable[Number(item.id)] ? 'pointer-events-none' : ''}`}>
                            <TodoItem
                                finished={item.finish}
                                contentEditable={contentEditable[Number(item.id)]}
                                className={`${contentEditable[Number(item.id)] === true ? '!bg-white !text-black' : `${editActive && !contentEditable[Number(item.id)] ? '!bg-light-gray' : '!bg-red-500'}`} ${contentEditable[Number(item.id)] ? '!pr-[70px]' : '!pr-10'}`}
                                onInput={e => setDescription(e.currentTarget.textContent || '')}
                                onKeyDown={(e: React.KeyboardEvent<HTMLDivElement>) => {
                                    if (e.key === "Enter") {
                                        e.preventDefault();
                                        handleUpdateItem(item.id as string)
                                    }
                                }}
                            >
                                {item.description}
                            </TodoItem>
                            {
                                !contentEditable[Number(item.id)] ?
                                    <div className={`absolute text-white right-2 cursor-pointer pr-1`} onClick={() => handleEdit(item.id as string)}>
                                        {editIcon}
                                    </div>
                                    :
                                    handleSavingIcon(item.id as string)
                            }

                        </div>
                        <div className='flex items-center gap-x-3'>
                            {loading.isFinish[Number(item.id)] ?
                                <div className="text-red-500 w-7 h-7">{loadingIcon('#EF4444')}</div>
                                :
                                <div onClick={() => handleCheck(item.id as string)} className={`text-red-500 ${editActive ? 'pointer-events-none !text-light-gray' : ''}`}>
                                    {item.finish ? checkedIcon : uncheckedIcon}
                                </div>
                            }
                            {
                                loading.isDelete[Number(item.id)] ?
                                    <div className="text-red-500 w-7 h-7">{loadingIcon('#EF4444')}</div>
                                    :
                                    <div onClick={() => handleDelete(item.id as string)} className={`text-red-500 ${editActive ? 'pointer-events-none !text-light-gray' : ''}`}>{deleteIcon}</div>
                            }
                        </div>
                    </div>
                )
            })}
        </div >
    )
}

export default EmergencyWorkSection