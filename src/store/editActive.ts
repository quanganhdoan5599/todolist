import { create } from 'zustand'
import { devtools } from 'zustand/middleware'
import { immer } from 'zustand/middleware/immer'

interface State {
  editActive: boolean
}
interface Action {
  setEditActive: (value: boolean) => void
}
export const useEditActiveStore = create(devtools(immer<State & Action>(
  set => ({
    editActive: false,
    setEditActive: value => set((state) => { state.editActive = value }),
  }),
), {
  name: 'editActive',
}))
