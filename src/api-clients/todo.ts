import { TWorkItem } from './../const';
import axiosClient from './axios-clients'

export const todoApi = {
    getTodoList() {
        return axiosClient.get(`/TodoList`)
    },
    getTodoItem(id: string) {
        return axiosClient.get(`/TodoList/${id}`)
    },
    addTodoItem(data: TWorkItem) {
        return axiosClient.post(`/TodoList`, data)
    },
    updateTodoItem(id: string, data: TWorkItem) {
        return axiosClient.put(`/TodoList/${id}`, data)
    },
    deleteTodoItem(id: string) {
        return axiosClient.delete(`/TodoList/${id}`)
    }
}